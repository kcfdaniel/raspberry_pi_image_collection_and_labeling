img_counter = 0

import cv2
import os
if not os.path.exists('images'):
    os.makedirs('images')

cam = cv2.VideoCapture(0)

# cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
# cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

while True:
    ret, frame = cam.read()
    cv2.imshow("window", frame)
    if not ret:
        break
    k = cv2.waitKey(1)

    if k%256 == 27 or k & 0xFF == ord('q'):
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img_name = "images/{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        print("{} written!".format(img_name))
        img_counter += 1

cam.release()

cv2.destroyAllWindows()